extern crate messenger;
extern crate tokio;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

#[test]
fn messenger_test() {
    let ping_called = Arc::new(AtomicBool::new(false));
    let pong_called = Arc::new(AtomicBool::new(false));

    let (ping, pong, messenger_future) = messenger::unbounded();

    let ping_handle = ping.on("message", |_| Some("Ping".to_owned()));
    let pong_handle = pong.on("message", |_| Some("Pong".to_owned()));

    let send_ping = ping.clone();
    let send_ping_called = ping_called.clone();

    let _ = ping.send("message", "Ping".to_owned(), move |data| {
        assert_eq!(data, ["Pong".to_owned()]);
        send_ping_called.store(true, Ordering::SeqCst);
        send_ping.close();
    });

    let send_pong_called = pong_called.clone();

    let _ = pong.send("message", "Pong".to_owned(), move |data| {
        assert_eq!(data, ["Ping".to_owned()]);
        send_pong_called.store(true, Ordering::SeqCst);
    });

    tokio::run(messenger_future);

    ping.off("message", ping_handle);
    pong.off("message", pong_handle);

    assert_eq!(ping_called.load(Ordering::SeqCst), true);
    assert_eq!(pong_called.load(Ordering::SeqCst), true);
}
