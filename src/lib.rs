#![feature(integer_atomics)]

#[macro_use]
extern crate futures;
extern crate serde;
#[macro_use]
extern crate serde_derive;

mod message;
mod messenger;
mod receiver;
mod sender;
mod unbounded;

pub(crate) use self::message::{Message, MessageKind};
pub use self::messenger::Messenger;
pub use self::receiver::Receiver;
pub use self::sender::Sender;
pub use self::unbounded::unbounded;
