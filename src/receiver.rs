use futures::Stream;
use futures::sync::mpsc;

pub trait Receiver: Stream {
    fn close(&mut self);
}

impl<T> Receiver for mpsc::UnboundedReceiver<T> {
    #[inline]
    fn close(&mut self) {
        mpsc::UnboundedReceiver::close(self);
    }
}
