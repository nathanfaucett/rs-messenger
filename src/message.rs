#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum MessageKind<T> {
    Close,
    RequestClose,
    Callback(Vec<T>),
    Data(String, T),
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Message<T> {
    id: u32,
    kind: MessageKind<T>,
}

impl<T> Message<T> {
    #[inline]
    pub fn new_close(id: u32) -> Self {
        Message {
            id: id,
            kind: MessageKind::Close,
        }
    }
    #[inline]
    pub fn new_request_close(id: u32) -> Self {
        Message {
            id: id,
            kind: MessageKind::RequestClose,
        }
    }
    #[inline]
    pub fn new_callback(id: u32, data: Vec<T>) -> Self {
        Message {
            id: id,
            kind: MessageKind::Callback(data),
        }
    }
    #[inline]
    pub fn new(id: u32, name: String, data: T) -> Self {
        Message {
            id: id,
            kind: MessageKind::Data(name, data),
        }
    }

    #[inline]
    pub fn id(&self) -> u32 {
        self.id
    }
    #[inline]
    pub fn kind(&self) -> &MessageKind<T> {
        &self.kind
    }
    #[inline]
    pub fn take_kind(self) -> MessageKind<T> {
        self.kind
    }
}
