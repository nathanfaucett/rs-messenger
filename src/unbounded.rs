use futures::sync::mpsc;
use futures::Future;

use super::Messenger;

/// creates server client messengers and a future, returns (ServerMessenger, ClientMessenger, Future)
#[inline]
pub fn unbounded<T>() -> (
    Messenger<T>,
    Messenger<T>,
    impl Future<Item = (), Error = ()>,
)
where
    T: 'static,
{
    let (server_sender, server_receiver) = mpsc::unbounded();
    let (client_sender, client_receiver) = mpsc::unbounded();

    let (server, server_future) = Messenger::new(server_sender, client_receiver);
    let (client, client_future) = Messenger::new(client_sender, server_receiver);

    let future = server_future
        .join(client_future)
        .map(|_| ())
        .map_err(|_| ());

    (server, client, future)
}
