use futures::sync::mpsc;

pub trait Sender {
    type Item;

    fn send(&self, Self::Item) -> Result<(), Self::Item>;
}

impl<S, T> Sender for Box<S>
where
    S: ?Sized + Sender<Item = T>,
{
    type Item = T;

    #[inline]
    fn send(&self, item: Self::Item) -> Result<(), Self::Item> {
        (&**self).send(item)
    }
}

impl<T> Sender for mpsc::UnboundedSender<T> {
    type Item = T;

    #[inline]
    fn send(&self, item: Self::Item) -> Result<(), Self::Item> {
        match mpsc::UnboundedSender::unbounded_send(self, item) {
            Ok(_) => Ok(()),
            Err(e) => Err(e.into_inner()),
        }
    }
}
