rs-messenger
=====

two way messenger

```rust
extern crate messenger;
extern crate tokio;

use messenger::unbounded_channel;
use tokio::executor::current_thread;

fn main() {
    let (ping, pong, future) = unbounded_channel();

    let _ = ping.on("message", |m| {
        println!("Ping received {:?}", m);
        Some("Pong".to_owned())
    });

    let p = pong.clone();
    let _ = pong.send("message", "Pong".to_owned(), move |data| {
        println!("Pong callback {:?}", data);
        p.close();
    });

    current_thread::run(move |_| {
        let _ = current_thread::spawn(future);
    });
}

```
